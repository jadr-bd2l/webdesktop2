import React from "react";
import { render } from "react-dom";

import 'bootstrap/dist/css/bootstrap.min.css';
import ApplicationQuestionnaire from './ApplicationQuestionnaire'
//plus-square

export class ApplicationLauncher extends React.Component{
  constructor() 
  {
    super();
    this.state = {
    directory:{}
    
    };
  
 
  }
  
  
  
  render() {
  if (this.props.type=="questionnaire")
    {
    return (
      <div>
      <ApplicationQuestionnaire
       id={this.props.id}
       data={this.props.data}
      />
      </div>      
    );
    }
  else
  {
    return (
    <div>
    Application: {this.props.type}
    </div>
    );
  }
} 
}
  
   

import React, { Component } from "react";
import * as Survey from "survey-react";

import { addItem,updateItem,setItems,killProcess,updateProcess } from "./actions.js";
import { connect } from "react-redux";


import "survey-react/survey.css";
import SurveyEditor from "./SurveyEditor";
import logo from "./logo.png";
//import "./App.css";
import "bootstrap/dist/css/bootstrap.css";

import "jquery-ui/themes/base/all.css";
import "nouislider/distribute/nouislider.css";
import "select2/dist/css/select2.css";
import "bootstrap-slider/dist/css/bootstrap-slider.css";

import "jquery-bar-rating/dist/themes/css-stars.css";

import jQuery, * as $ from 'jquery/dist/jquery.js';
import "jquery-ui/ui/widgets/datepicker.js";
import "select2/dist/js/select2.js";
import "jquery-bar-rating";

import * as widgets from "surveyjs-widgets";

widgets.icheck(Survey, $);
widgets.select2(Survey, $);
widgets.inputmask(Survey);
widgets.jquerybarrating(Survey, $);
widgets.jqueryuidatepicker(Survey, $);
widgets.nouislider(Survey);
widgets.select2tagbox(Survey, $);
widgets.signaturepad(Survey);
widgets.sortablejs(Survey);
widgets.ckeditor(Survey);
widgets.autocomplete(Survey, $);
widgets.bootstrapslider(Survey);

class ConnectedApplicationQuestionnaire  extends React.Component{
  
    constructor() 
  {
    super();
    this.state = {
   
    
    };
   this.save=this.save.bind(this);
   this.load=this.load.bind(this);
 
  }
  load()
  {
      console.log("loading data:",this.props.desktopItems);
      
      var item=this.props.desktopItems.find(item=>item.id==this.props.id);
    
      console.log("data=",item.data);
      return item.data;
  }
  save(data)
  {
      console.log("SAVING into",this.props.id,data);
      var item=this.props.desktopItems.find(item=>item.id==this.props.id);
      var changed = Object.assign(
								{
								}, item, {
								
									
									data: data
								}); 
                            	this.props.dispatch(
								{
									type: 'UPDATE_ITEM',
									instance: changed
								});
      
  }
  
  componentWillMount() {
    import("icheck");
    window["$"] = window["jQuery"] = $;
  }

  onValueChanged(result) {
    console.log("value changed!");
  }

  onComplete(result) {
    console.log("Complete! " + result);
  }

  render() {
    Survey.Survey.cssType = "bootstrap";
    var data=this.load();
    var model = new Survey.Model(this.json);
    return (
     
    
        <div class="application">
          <SurveyEditor 
           id={this.props.id}
           data={this.props.data}
           save={this.save}
          />
       </div>
      
      
    );
  }
}

const mapStateToProps = state =>{
return  {
    desktopItems: state.desktopItems,
    desktopMenus: state.desktopMenus,
    desktopProcesses: state.desktopProcesses
    
        }
	};

const ApplicationQuestionnaire = connect(mapStateToProps)(ConnectedApplicationQuestionnaire);

export default ApplicationQuestionnaire;
 
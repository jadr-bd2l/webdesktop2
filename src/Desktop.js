import React from "react";
import { render } from "react-dom";
import DesktopViewPort  from "./DesktopViewPort";
//import {Button,Modal, ModalHeader, ModalBody, ModalFooter }  from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFolderPlus,faProjectDiagram} from '@fortawesome/free-solid-svg-icons';
import { addItem,updateItem,setItems,killProcess,updateProcess } from "./actions.js";
import { connect } from "react-redux";
import {DesktopWindow} from "./DesktopWindow";
import {DesktopFileWindow} from "./DesktopFileWindow";
import DesktopTaskBar from "./DesktopTaskBar"
import {DesktopContextMenu} from './DesktopContextMenu';
import{showMenu}from 'react-contextmenu/modules/actions';
import './contextMenu.css';
import {Button}  from 'reactstrap';

import 'bootstrap/dist/css/bootstrap.min.css';

var store=window.store;
var mouse={x:0,y:0};
//plus-square

window.onmousemove = logMouseMove;
function logMouseMove(event)
{
    var x = event.clientX;
	var y = event.clientY;
    mouse.x=x;
    mouse.y=y;
}
class ConnectedDesktop extends React.Component{
  constructor() 
  {
    super();
    this.state = {
    directory:{},
    init:0
    
    };
   this.selectedItem=-1;
   this.closeWindow=this.closeWindow.bind(this);
   this.maximizeWindow=this.maximizeWindow.bind(this);
   this.minimizeWindow=this.minimizeWindow.bind(this);
   this.showContextMenu=this.showContextMenu.bind(this);
   this.navigateWindow=this.navigateWindow.bind(this);
   this.contextMenuItemClick=this.contextMenuItemClick.bind(this);
   
   this.findItemChildren=this.findItemChildren.bind(this);
   this.findItemRoot=this.findItemRoot.bind(this);
   this.navigatePath=this.navigatePath.bind(this);
   this.dedupe=this.dedupe.bind(this);
   this.maximizeProcess=this.maximizeProcess.bind(this);
   this.minimizeProcess=this.minimizeProcess.bind(this);
   this.closeProcess=this.closeProcess.bind(this);
   this.createProcess=this.createProcess.bind(this);
   this.getChildren=this.getChildren.bind(this);
  }
  
  getChildren(id)
  {
    console.log("getChildren(",id,")");
    var newItems=[];
    console.log("getDirectory",this.props.desktopItems,id);
    if (this.props.desktopItems)
    for (var i=0;i<this.props.desktopItems.length;i++)
        {
            if (this.props.desktopItems[i].parentId==id)
            {
                console.log ("Adding ",this.props.desktopItems[i]);
                newItems.push(this.props.desktopItems[i]);
            }
        }
        console.log("NewItems=",newItems)
  return newItems;
  }
  
  
  findItemChildren(desktopItems,itemId)
  {
   var item=this.props.desktopItems.find(item=>item.id==itemId);
   var nextItems=this.getChildren(item.id);
   return (nextItems);
    
  }
  
   findItemRoot(desktopItems,item,root)
  {
  
    var parentId=item.parentId;
    var root=item;
    for (var i=0;i<this.props.desktopItems.length;i++)
        {
            var obj=this.props.desktopItems[i];
            if (obj.id==parentId)
                {
                    parentId=obj.parentId;
                    root=obj;
                }
        }
    return root;
  
    
  }
  dedupe(navStatus)
  {
    var idx=[];
    var newNavStatus=[];
    for (var i=0;i<navStatus.length;i++)
        {
            if (!idx[navStatus[i].id])
                {
                    newNavStatus.push(navStatus[i]);
                    idx[navStatus[i].id]=1;
                }
        }
  return (newNavStatus);      
  }
  navigatePath(itemId)
  {
    console.log(" navigatePath=",itemId);
    var item=this.props.desktopItems.find(item=>item.id==itemId);
    var windowItem=this.findItemRoot(this.props.desktopItems,item,itemId);
    console.log("NavItem=",windowItem);
  
    
    var navStatus=this.dedupe(windowItem.navStatus);
    console.log("navItem=",windowItem,navStatus);
    if (navStatus.length==1)
    {
        var navStatus=[];
    
    }
    else
    {
      
       navStatus.pop();
    }
    
      var changed = Object.assign(
								{
								}, windowItem, {
								
									
									navStatus: navStatus
								}); 
                            	this.props.dispatch(
								{
									type: 'UPDATE_ITEM',
									instance: changed
								});
    
  }
  navigateWindow(windowId,itemId,noUpdateFlag)
    {
   
    var parent=this.props.desktopItems.find(item=>item.id==windowId);
    var item=this.props.desktopItems.find(item=>item.id==itemId);
   
    var navItems=this.getChildren(itemId);
    console.log("navItems=",navItems);
    var navPath={items:navItems,id:itemId,name:item.name};
    var navStatus=parent.navStatus;
    navStatus.push(navPath);
    if (noUpdateFlag==true)
    {
      //navStatus.pop();
    }
    
    var changed = Object.assign(
								{
								}, parent, {
								
									
									navStatus: navStatus
								}); 
                            	this.props.dispatch(
								{
									type: 'UPDATE_ITEM',
									instance: changed
								});
    console.log("navigateWindow=",changed);                            
   
    }
  showContextMenu(name,id)
  {
    var item=this.props.desktopItems.find(item=>item.id==id);
    if (item.fileStatus=="locked") return;
   this.selectedItem=id;
   var x=mouse.x;
   var y=mouse.y;
    console.log("showContextMenu","CM"+name,x,y);
    showMenu(
					{
						position: {
							x, y
						},
						target: this.ref,
						id: "CM"+name
					});
  }
  contextMenuItemClick(value)
  {
    var tag=value.getElementsByTagName('data');
    var id=tag[0].attributes[0].nodeValue;
    console.log("name=",id,"item=",this.selectedItem);
  }
  createProcess(item)
  {
    var item2=this.props.desktopItems.find(item2=>item2.id==item.id);
    
    if (item2.fileStatus!="locked")
   {
    var process={
        
        windowShow:true,
        windowStatus:"maximized",
        title:item.name,
        data:item.data,
        type: item.type,
        id:item.id
    };
    	this.props.dispatch(
								{
									type: 'ADD_PROCESS',
									instance: process
								});
                                
    
    
    console.log("fileStatus item=",item);
   var changed = Object.assign(
								{
								}, item, {
									fileStatus: "locked"
								}); 
                            	this.props.dispatch(
								{
									type: 'UPDATE_ITEM',
									instance: changed
								});      
         } 
         else
         {
            alert("Already Open");
         }
  }
  closeProcess(id)
  {
   
   
    var process=this.props.desktopProcesses.find(process=>process.id==id);
    var item=this.props.desktopItems.find(item=>item.id==process.id);
    console.log("PROCESS=",process) ;
    
    
       var changed = Object.assign(
								{
								}, process, {
								
									
									
								}); 
                            	this.props.dispatch(
								{
									type: 'KILL_PROCESS',
									instance: changed
								});
    
   var changed = Object.assign(
								{
								}, item, {
								
									
									fileStatus: "normal"
								}); 
                            	this.props.dispatch(
								{
									type: 'UPDATE_ITEM',
									instance: changed
								});     
                          
    console.log("Checking:",item,id);      
    var windowItem=this.findItemRoot(this.props.desktopItems,item,id);
     var navStatus=windowItem.navStatus;
     var navId=navStatus[navStatus.length-1].id;
    console.log("NavItem=",navId);
  
    
   
  this.navigateWindow(windowItem.id,navId,true) ;
                           
                                
  }
  minimizeProcess(id)
  {
     console.log("props=",this.props);
    var process=this.props.desktopProcesses.find(process=>process.id==id);
    var changed = Object.assign(
								{
								}, process, {
								
									
									windowStatus: "minimized"
								}); 
                            	this.props.dispatch(
								{
								    type: 'UPDATE_PROCESS',
									instance: changed
								});
  }
  maximizeProcess(id)
  {
     console.log("props=",this.props);
    var process=this.props.desktopProcesses.find(process=>process.id==id);
   
      if (process.windowStatus=="normal")
        var maximize="maximized";
    else
        var maximize="normal";
    var changed = Object.assign(
								{
								}, process, {
								
									
									windowStatus: maximize
								}); 
                            	this.props.dispatch(
								{
								    type: 'UPDATE_PROCESS',
									instance: changed
								});
  }
  closeWindow(id)
  {
    console.log("props=",this.props);
    var item=this.props.desktopItems.find(item=>item.id==id);
    var changed = Object.assign(
								{
								}, item, {
								
									navStatus :[],
									windowShow: false
								}); 
                            	this.props.dispatch(
								{
									type: 'UPDATE_ITEM',
									instance: changed
								});
  }
  minimizeWindow(id)
  {
     var item=this.props.desktopItems.find(item=>item.id==id);
    var changed = Object.assign(
								{
								}, item, {
								
									
									windowStatus: "minimized"
								}); 
                            	this.props.dispatch(
								{
									type: 'UPDATE_ITEM',
									instance: changed
								});
  }
  maximizeWindow(id)
  {
    console.log("props=",this.props);
   var item=this.props.desktopItems.find(item=>item.id==id);
    if (item.windowStatus=="normal")
        var maximize="maximized";
    else
        var maximize="normal";
    var changed = Object.assign(
								{
								}, item, {
								
									
									windowStatus: maximize
								}); 
                            	this.props.dispatch(
								{
									type: 'UPDATE_ITEM',
									instance: changed
								});
  }
  render() {
    console.log("PROPS=",this.props.desktopItems);
  
    return (
       <div className="App">
        
        
      
     

	<div class="abs" id="wrapper">
  <div class="abs" id="bar_top">

	    
	      

	     <Button
         color="black"
         onclick={console.log("click")}
         >
        <img src='fulllogo.png' width="82px" height="32px"/>
         
         </Button>

	  

	    
        
          
	   
	      

	   

	    

	   
          </div>
     	

	  <div class="abs" id="desktop">
      <div class="abs"	id="wallpaper">
      
      <DesktopTaskBar/>
          
         
        <DesktopViewPort
        items={this.props.desktopItems}
        windowId={-1}
        showContextMenu={this.showContextMenu}
        navigateWindow={this.navigateWindow}
         mode="root"
        
        />
          {this.getChildren(-1).map(item => {
            console.log("ITEM=",item,item["type"]);
            if (item["type"] && item.parentId==-1)
            {
            var menu=this.props.desktopMenus[item.type].menu;
            var navStatus=item.navStatus;
           
         return(
         
            <DesktopWindow
           
            id={item.id}
            title={item.name}
            
            show={item.windowShow}
            menu={menu}
            mode="sub"
            navStatus={item.navStatus}
            showContextMenu={this.showContextMenu}
            navigatePath={this.navigatePath}
            navigateWindow={this.navigateWindow}
            createProcess={this.createProcess}
            status={item.windowStatus}
            closeWindow={this.closeWindow}
            maximizeWindow={this.maximizeWindow}
            minimizeWindow={this.minimizeWindow}
            windowId={item.id}
            /> 
      
   
    );
    }
    })}
    
    {this.props.desktopProcesses.map(process=>{
        console.log("P=",this.props.desktopProcesses);
        if (process)
        if (process.windowStatus!="dead")
        return(
          <DesktopFileWindow
           
            id={process.id}
            title={process.title}
            type={process.type}
            data={process.data}
            show={process.windowShow}
            status={process.windowStatus}
            closeWindow={this.closeProcess}
            maximizeWindow={this.maximizeProcess}
            minimizeWindow={this.minimizeProcess}
            /> 
        );
        
    })}
    
     {this.props.desktopMenus.map(menu => {   
        var menuItems=this.props.desktopMenus[menu.type].contextMenu;
        console.log("menuItems=",menuItems,this.props.desktopMenus);
         return(
         <DesktopContextMenu
         id={menu.type}
         menuItems={menuItems}
         contextMenuItemClick={this.contextMenuItemClick}
         />
         );
      })}
    
      </div>
      </div>
      
      </div>
      </div>
    );
  }
  
 }
 
const mapStateToProps = state =>{
return  {
    desktopItems: state.desktopItems,
    desktopMenus: state.desktopMenus,
    desktopProcesses: state.desktopProcesses
    
        }
	}

const Desktop = connect(mapStateToProps)(ConnectedDesktop);
export default Desktop;
 
import React from "react";
import { render } from "react-dom";
import {Button}  from 'reactstrap';
import {ContextMenu, MenuItem, ContextMenuTrigger}from "react-contextmenu";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import 'bootstrap/dist/css/bootstrap.min.css';
//plus-square

export class DesktopContextMenu extends React.Component{
  constructor() 
  {
    super();
    this.state = {
   
    
    };
   this.menuClick=this.menuClick.bind(this);
 
  }
  
  menuClick(e)
  {
    console.log(e.target)
    this.props.contextMenuItemClick(e.target);
  }
  render() {
  console.log("props=",this.props);
     console.log("this.props.menuItems=",this.props.menuItems)

    return (
       <div class="context">
        
	   <ContextMenu id={"CM"+this.props.id} >
        {this.props.menuItems.map(item => {   
         
         return(
          <MenuItem  id={item.name} onClick={this.menuClick}>
                  {item.label}
                  <data value={item.name}/>
        
         </MenuItem>
         );
      })}
       
       
      </ContextMenu>
       </div>
     
        
    );
    
} 
}

//<FontAwesomeIcon icon={faEdit} color="SteelBlue" size='1.5x'/>&nbsp; 

   
 


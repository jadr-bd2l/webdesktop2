import React from "react";
import { render } from "react-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import ReactModal from 'react-modal-resizable-draggable';
import './reactModal.css';
import {DesktopWindow} from './DesktopWindow';

import {Rnd} from 'react-rnd';
import { connect } from "react-redux";
import { addItem,updateItem } from "./actions.js";
 class ConnectedDesktopFile extends React.Component{
    
  constructor() 
  {
    super();
    this.state = {
    
    
    };
   
    this.handleContextMenu=this.handleContextMenu.bind(this);
    this.openWindow = this.openWindow.bind(this);
    this.fileClick=this.fileClick.bind(this);
    
  }
  
  fileClick()
  {
    //this.props.item.data;
    console.log("FILE DATA=",this.props.item.data);
  }
  
  openWindow() {
   
   this.props.createProcess(this.props.item);
    }
  
  
  
  handleContextMenu()
  {
  
   this.props.showContextMenu(this.props.item.type,this.props.item.id);
  }
  render() {
  
  console.log("STATUS=",this.props.item.fileStatus);
  var x=parseInt(this.props.item.left.replace("px",""));
    var y=parseInt(this.props.item.top.replace("px",""));
    var x2=x-this.props.xOffset;
    if (this.props.mode!="sub")
    var left=x2+"px";
    else
    var left=x;
    
    var item=this.props.desktopItems.find(item=>item.id==this.props.item.id);
    
    if (item.fileStatus!="locked")
        {
            var color="SteelBlue";
            
        }
    else
        {
           var color="red"; 
        }
       if (true)
    {
    return (
    <div>
    <Rnd
     
    
 
   
      
    >
      <div 
       onContextMenu={this.handleContextMenu}
      >
       <div  
       class={this.props.className}
        
        style={{'left':left,'top':this.props.item.top}} 
        onClick={this.fileClick}
        onDoubleClick={this.openWindow}
      
       >

	    <FontAwesomeIcon icon={this.props.icon} color={color} size='4x'/>
       <div> 
	   {this.props.item.name}
       </div>
        </div>
        
        
       
      
        
       
      </div>
     
     </Rnd>
       </div>
      
    );
    
  }
  else
  {
     return (
    <div>
   
      <div 
       onContextMenu={this.handleContextMenu}
      >
       <div  
       class={this.props.className}
        
        style={{'left':left,'top':this.props.item.top}} 
       
        onDoubleClick={this.openWindow}
        onClick={this.fileClick}
       >

	    <FontAwesomeIcon icon={this.props.icon} color={color} size='4x'/>
       <div> 
	   {this.props.item.name}
       </div>
        </div>
        
        
       
      
        
       
      </div>
     

       </div>
      
    );
  }
  }
 }
 
 const mapStateToProps = state =>{
return  {
    desktopItems: state.desktopItems,
     desktopMenus: state.desktopMenus
    
        }
	}

const DesktopFile = connect(mapStateToProps)(ConnectedDesktopFile);
export default DesktopFile;
 
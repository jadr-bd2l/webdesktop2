import React, { Component } from 'react';
import { render } from "react-dom";
import {Rnd} from 'react-rnd';
import { Window, TitleBar, Text,NavPane, NavPaneItem } from 'react-desktop/windows';

import { Nav,Navbar,NavbarBrand, NavItem, Dropdown, DropdownItem, DropdownToggle, DropdownMenu, NavLink } from 'reactstrap';


import 'bootstrap/dist/css/bootstrap.min.css';
import {ApplicationLauncher} from "./ApplicationLauncher";
import $ from "jquery";


export class DesktopFileWindow extends React.Component{
  constructor() 
  {
    super();
    this.state = {
     width:600,
     height:400,
     position:{x:150,y:205},
     status:"normal",
     doFocus:0
    
    
    
    };
 
  this.tick=this.tick.bind(this);
  this.onCloseClick=this.onCloseClick.bind(this);
  this.onMinimizeClick=this.onMinimizeClick.bind(this);
  this.onMaximizeClick=this.onMaximizeClick.bind(this);
  this.focus=this.focus.bind(this);

  
 // this.renderIcon=this.renderIcon.bind(this);	
        }
    componentDidMount() {
     this.timerID = setInterval(
      () => this.tick(),
      500
    );

  }
  tick()
  {
    if (this.state.doFocus==1)
        {
        this.focus(); 
        this.state.doFocus=0;
        }
    //console.log("h=",this.state.height,"w=",this.state.width);
  }
  
   focus()
  {
   
    var undefined;
    var max=0;
   
    var id=this.props.id;
  
    console.log("FOCUS=",this.props,id);
    
    
    $('.windowBounds').each(function(i, obj) {
        var zIndex=parseInt($( obj ).css( "z-index" ));
        if (zIndex>max)
            {max=zIndex;}
        
     });
    $('.windowBounds').each(function(i, obj) {
    
    var zIndex=$( obj ).css( "z-index" );
    console.log("obj=",obj.id,"window"+id,zIndex);
    if ("window"+id==obj.id)
        {
            $( obj ).css( "z-index" ,max+1);
        }
    });
  }
  
  onCloseClick(){
   this.props.closeWindow(this.props.id);
  }
  onMinimizeClick()
  {
  this.props.minimizeWindow(this.props.id);  
  this.focus();
  }
  onMaximizeClick()
  {
   this.focus(); 
   this.props.maximizeWindow(this.props.id);  
  }
 
  
  

  

  
// <DropdownItem divider />
  render() {
    
  console.log("navStatus=",this.props.navStatus);
   if (this.props.show==true && this.props.status!="minimized")  
 {
 var init=0;
  
 if (this.state.status=="normal" && this.props.status=="maximized" )
 {
   
    this.state.doFocus=1;   
    this.state.status="maximized";
    init=1;
 }
 if (this.state.status=="maximized" && this.props.status=="normal" )
 {
    this.state.doFocus=1;  
    this.state.status="normal";
    init=1;
 } 
 
 if (init==1 || this.state.status=="")
 {
 if (this.props.status=="maximized")
 {
    
    var width=window.innerWidth;
    var height=window.innerHeight-83;
    this.state.width=width;
    this.state.height=height;
    var x=0;
    var y=30;
     this.state.position.x=0;
    this.state.position.y=0;
    
 }  
 else
 {
  
    var x=150;
    var y=205;
    
     this.setState({
                width: 800,
                height: 450,
                position:{x:x,y:y}
                });
 } 
}
console.log("WindowId=",this.props.windowId);

var windowId=this.props.windowId;
 if (init==1)
 {
 if (this.props.status!="maximized")
 {
 return(
 <div class="windowBounds" id={"window"+this.props.id} onClick={this.focus}>
     <Rnd
      default={{
        x: x,
        y: y,
        width: width,
        height: height,
      }}
      size={{ width: this.state.width,  height: this.state.height }}
      position={{ x: this.state.position.x, y: this.state.position.y }}
      onResize={(e, direction, ref, delta, position) => {
        console.log("position=",position);
      
            this.setState({
                width: ref.style.width,
                height: ref.style.height,
                position:position
                });
  }}
        minWidth={600}
      minHeight={350}
     
    >
    
      <Window
        color={this.props.color}
          theme="light"
        class="DesktopApplication"
        chrome={true}
        height={this.state.height}
        width={this.state.width}
        padding="12px"
      >
       
        <TitleBar title={this.props.title} controls
        onCloseClick={this.onCloseClick}
        background= "royalblue"
        onMaximizeClick={this.onMaximizeClick}
        onMinimizeClick={this.onMinimizeClick}
        />
     
            <div class="itemViewPort">
            <ApplicationLauncher
            type={this.props.type}
            id={this.props.id}
            />
            </div>
       
      
     
      </Window>
    </Rnd>
 </div>
 );
 }
 else
 {
  
     
 return(
<div class="windowBounds" id={"window"+this.props.id} onClick={this.focus}>
     <Rnd
      default={{
        x: 0,
        y: 0,
        width: this.state.width,
        height: this.state.height,
      }}
      size={{ width: this.state.width,  height: this.state.height }}
      position={{ x: 0, y: 0 }}
     disableDragging
      minWidth={600}
      minHeight={350}
   
    >
    
      <Window
        color={this.props.color}
        theme="light"
        class="DesktopApplication"
        chrome={true}
        height={this.state.height}
        width={this.state.width}
        padding="12px"
      >
       
        <TitleBar title={this.props.title} controls
        onCloseClick={this.onCloseClick}
        background= "royalblue"
        onMaximizeClick={this.onMaximizeClick}
        onMinimizeClick={this.onMinimizeClick}
        />
      
         <div class="itemViewPort">
         <ApplicationLauncher
            type={this.props.type}
               data={this.props.data}
               id={this.props.id}
            />
 </div>
      
     
      </Window>
    </Rnd>
 </div>
 );   
 }
 }
 else
 {
 return(
  <div class="windowBounds" id={"window"+this.props.id} onClick={this.focus}>
    <Rnd
       onResize={(e, direction, ref, delta, position) => {
        console.log("position=",position);
      
            this.setState({
                width: ref.style.width,
                height: ref.style.height,
                  position:position
                });
  }}
        minWidth={600}
      minHeight={350}
     
    >
      <Window
        color={this.props.color}
      
        class="DesktopApplication"
         theme="light"
        chrome={true}
        height={this.state.height}
        width={this.state.width}
        padding="12px"
      >  
        <TitleBar title={this.props.title} controls
        onCloseClick={this.onCloseClick}
        background= "royalblue"
        onMaximizeClick={this.onMaximizeClick}
        onMinimizeClick={this.onMinimizeClick}
        />
        
   <div class="itemViewPort">
    <ApplicationLauncher
            type={this.props.type}
            data={this.props.data}
            id={this.props.id}
            />
 </div>
      </Window>
    </Rnd>
 </div>
 );   
 }
 }
 if (this.props.show==false || this.props.status=="minimized")  
 return(
 <div>
 </div>
 );

			
		
   
   
  }
  
 }
  
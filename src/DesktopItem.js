import React from "react";
import { render } from "react-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { 
    faFolder,
    faProjectDiagram,
    faDatabase,
    faClipboardList,
    faChartLine,
    faAddressBook,
    faEnvelopeOpenText,
    faExclamationTriangle,
    faFileImage,
    faUsersCog,
    faTasks,
    faEdit} from '@fortawesome/free-solid-svg-icons';

import {DesktopFolder}  from "./DesktopFolder";
import DesktopFile  from "./DesktopFile";
import {DesktopProject}  from "./DesktopProject";
import {ContextMenu, MenuItem, ContextMenuTrigger}from "react-contextmenu";
import{showMenu}from 'react-contextmenu/modules/actions';
import './contextMenu.css';
import {Rnd} from 'react-rnd';
export class DesktopItem extends React.Component{
  constructor() 
  {
    super();
    this.state = {
    
    
    };
    this.loadDataset=this.loadDataset.bind(this);
    this.handleContextMenu=this.handleContextMenu.bind(this);
  }
  loadDataset(dashboardId)
  {
   
  }
  handleContextMenu()
  {
  
    var x=parseInt(this.props.item.left.replace("px",""))+50;
    var y=parseInt(this.props.item.top.replace("px",""))+40;
    showMenu(
					{
						position: {
							x, y
						},
						target: this.ref,
						id: this.props.item.type,
					});
  }
  render() {
     var x=this.props.item.left;
  x=x.replace("px","");
  var y=this.props.item.top;
  y=y.replace("px","");
  var xOffset=0;
   if (this.props.mode!="sub")
   var className="abs icon"
   else
   {
    var className="abs iconSub"
    xOffset=450;
   }
  
  
    if (this.props.item.type=="folder")
    return (
     
    
    
      <DesktopFolder
      item={this.props.item}
      windowId={this.props.windowId}
      showContextMenu={this.props.showContextMenu}
      openWindow={this.props.openWindow}
      navigateWindow={this.props.navigateWindow}
      className={className}
      xOffset={xOffset}
      mode={this.props.mode}
      />
      
   
    );
    if (this.props.item.type=="project")
    return (
      
     
      <DesktopProject
      item={this.props.item}
      showContextMenu={this.props.showContextMenu}
      openWindow={this.props.openWindow}
      navigateWindow={this.props.navigateWindow}
      className={className}
      mode={this.props.mode}
      windowId={this.props.windowId}
      xOffset={xOffset}
      />
       
	  
       
      
    );
   if (this.props.item.type=="dataset")
   return(
   
    <DesktopFile
      item={this.props.item}
      icon={faDatabase}
    fileStatus={this.props.fileStatus}
      createProcess={this.props.createProcess}
      showContextMenu={this.props.showContextMenu}
      className={className}
      mode={this.props.mode}
      windowId={this.props.windowId}
      xOffset={xOffset}
      />
   
  
   
   );
    if (this.props.item.type=="questionnaire")
   return(
  
    <DesktopFile
      item={this.props.item}
      icon={faClipboardList}
      showContextMenu={this.props.showContextMenu}
      createProcess={this.props.createProcess}
      fileStatus={this.props.fileStatus}
      className={className}
      mode={this.props.mode}
      windowId={this.props.windowId}
      xOffset={xOffset}
      />
  
   
   
   );
      if (this.props.item.type=="dashboard")
   return(
   
    <DesktopFile
      item={this.props.item}
      icon={faChartLine}
      showContextMenu={this.props.showContextMenu}
      createProcess={this.props.createProcess}
     fileStatus={this.props.fileStatus}
      className={className}
      mode={this.props.mode}
      windowId={this.props.windowId}
      xOffset={xOffset}
      />
   
  
   
   );
    if (this.props.item.type=="mailingList")
   return(
   
    <DesktopFile
      item={this.props.item}
      icon={faAddressBook}
       showContextMenu={this.props.showContextMenu}
      createProcess={this.props.createProcess}
     fileStatus={this.props.fileStatus}
      className={className}
      mode={this.props.mode}
      windowId={this.props.windowId}
      xOffset={xOffset}
      />
   
   
   );
     if (this.props.item.type=="email")
   return(
   
    <DesktopFile
      item={this.props.item}
      icon={faEnvelopeOpenText}
      showContextMenu={this.props.showContextMenu}
      createProcess={this.props.createProcess}
     fileStatus={this.props.fileStatus}
      className={className}
      mode={this.props.mode}
      windowId={this.props.windowId}
      xOffset={xOffset}
      />
   
   
   
   );
     if (this.props.item.type=="alert")
   return(
   
     <DesktopFile
      item={this.props.item}
      icon={faExclamationTriangle}
      showContextMenu={this.props.showContextMenu}
      createProcess={this.props.createProcess}
     fileStatus={this.props.fileStatus}
      className={className}
      mode={this.props.mode}
      windowId={this.props.windowId}
      xOffset={xOffset}
      />
   
  
   
   );
     if (this.props.item.type=="image")
   return(
   
    <DesktopFile
      item={this.props.item}
      icon={faUsersCog}
       showContextMenu={this.props.showContextMenu} 
      createProcess={this.props.createProcess}
     fileStatus={this.props.fileStatus}
      className={className}
      mode={this.props.mode}
      windowId={this.props.windowId}
      xOffset={xOffset}
      />
   
   
   
   );
    if (this.props.item.type=="panel")
   return(
   
   <DesktopFile
      item={this.props.item}
      icon={faUsersCog}
   fileStatus={this.props.fileStatus}
      createProcess={this.props.createProcess}
      showContextMenu={this.props.showContextMenu}
      className={className}
      mode={this.props.mode}
      windowId={this.props.windowId}
      xOffset={xOffset}
      />
   
    
   
   );
  
     if (this.props.item.type=="quotas")
   return(
   
   <DesktopFile
      item={this.props.item}
      icon={faTasks}
       showContextMenu={this.props.showContextMenu}
      createProcess={this.props.createProcess}
     fileStatus={this.props.fileStatus}
      className={className}
      mode={this.props.mode}
      windowId={this.props.windowId}
      xOffset={xOffset}
      />
   
   
   
   );
  }
  
 }

  
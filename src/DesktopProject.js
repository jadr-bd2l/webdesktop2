import React from "react";
import { render } from "react-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFolder,faEdit,faProjectDiagram} from '@fortawesome/free-solid-svg-icons';

import ReactModal from 'react-modal-resizable-draggable';
import './reactModal.css';
import {DesktopWindow} from './DesktopWindow';

import {Rnd} from 'react-rnd';
export class DesktopProject extends React.Component{
    
  constructor() 
  {
    super();
    this.state = {
    
    
    };
    this.loadDataset=this.loadDataset.bind(this);
    this.handleContextMenu=this.handleContextMenu.bind(this);
    this.openWindow = this.openWindow.bind(this);
    
  }
  
  openWindow() {
  console.log("Props=",this.props);
    if (this.props.mode!="sub")
    this.props.openWindow(this.props.item.id);
    else
    {
   
    this.props.navigateWindow(this.props.windowId,this.props.item.id);
    }
    }
  
  
  loadDataset(dashboardId)
  {
   
  }
  handleContextMenu()
  {
  
   this.props.showContextMenu(this.props.item.type,this.props.item.id);
  }
  render() {
  
  var x=parseInt(this.props.item.left.replace("px",""));
    var y=parseInt(this.props.item.top.replace("px",""));
    var x2=x-this.props.xOffset;
    if (this.props.mode!="sub")
    var left=x2+"px";
    else
    var left=x;
       if (true)
    {
    return (
    <div>
    <Rnd
     
    
 
   
      bounds="window"
    >
      <div 
       onContextMenu={this.handleContextMenu}
      >
       <div  
       class={this.props.className}
        
        style={{'left':left,'top':this.props.item.top}} 
       
        onDoubleClick={this.openWindow}
      
       >

	    <FontAwesomeIcon icon={faProjectDiagram} color="SteelBlue" size='4x'/>
       <div> 
	   {this.props.item.name}
       </div>
        </div>
        
        
       
      
        
       
      </div>
     
     </Rnd>
       </div>
      
    );
    
  }
  else
  {
     return (
    <div>
   
      <div 
       onContextMenu={this.handleContextMenu}
      >
       <div  
       class={this.props.className}
        
        style={{'left':left,'top':this.props.item.top}} 
       
        onDoubleClick={this.openWindow}
      
       >

	    <FontAwesomeIcon icon={faProjectDiagram} color="SteelBlue" size='4x'/>
       <div> 
	   {this.props.item.name}
       </div>
        </div>
        
        
       
      
        
       
      </div>
     

       </div>
      
    );
  }
  }
 }
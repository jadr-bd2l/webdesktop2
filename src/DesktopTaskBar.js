import React from "react";
import { render } from "react-dom";

import { connect } from "react-redux";
import { addItem,updateItem } from "./actions.js";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {Button}  from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
//plus-square

export class ConnectedDesktopTaskBar extends React.Component{
  constructor() 
  {
    super();
    this.state = {
    directory:{}
    
    };
   this.deMinimize=this.deMinimize.bind(this);
   this.deMinimizeProcess=this.deMinimizeProcess.bind(this);
 
  }
  
  deMinimize(e)
  {
    console.log(e.target.id);
    var id=e.target.id;
     var item=this.props.desktopItems.find(item=>item.id==id);
    var changed = Object.assign(
								{
								}, item, {
								
									
									windowStatus: "Normal"
								}); 
                            	this.props.dispatch(
								{
									type: 'UPDATE_ITEM',
									instance: changed
								});
  }
  
  deMinimizeProcess(e)
  {
    console.log(e.target.id);
    var id=e.target.id;
     var process=this.props.desktopProcesses.find(process=>process.id==id);
    var changed = Object.assign(
								{
								}, process, {
								
									
									windowStatus: "Normal"
								}); 
                            	this.props.dispatch(
								{
									type: 'UPDATE_PROCESS',
									instance: changed
								});
  }
  
  render() {
  console.log("props=",this.props);
  var items=this.props.desktopItems;
  var processes=this.props.desktopProcesses;
    return (
       <div class="abs" id="bar_bottom">

	    <a class="float_right" href="#" id="show_desktop" title="Show Desktop">

	      <img src="/assets/images/icons/icon_22_desktop.png" />

	    </a>
         {items.map(item => { 
         if (item.windowShow==true)
         return(
         
            <Button
            
            color="secondary"
             id={item.id}
             onClick={this.deMinimize}
            
           
           
            >
            {item.name}
            </Button>
         );
         })}
	    {processes.map(process => { 
         if (process.windowStatus!="dead")
         return(
         
            <Button
            
            color="primary"
             id={process.id}
             onClick={this.deMinimizeProcess}
            
           
           
            >
            {process.title}
            </Button>
         );
         })}
          </div>
     
        
    );
    
} 
}
  
   
 const mapStateToProps = state =>{
return  {
    desktopItems: state.desktopItems,
     desktopMenus: state.desktopMenus,
     desktopProcesses:state.desktopProcesses
    
        }
	}

const DesktopTaskBar = connect(mapStateToProps)(ConnectedDesktopTaskBar);
export default DesktopTaskBar;
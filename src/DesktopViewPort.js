import React from "react";
import { render } from "react-dom";
import {DesktopItem}  from "./DesktopItem";

import { connect } from "react-redux";
import { addItem,updateItem } from "./actions.js";
//plus-square

export class ConnectedDesktopViewPort extends React.Component{
  constructor() 
  {
    super();
    this.state = {
    directory:{}
    
    };
  this.openWindow=this.openWindow.bind(this);
  
  this.getChildren=this.getChildren.bind(this);
  this.getRoot=this.getRoot.bind(this);
  }
  
  
  getRoot(item)
  {
    var parentId=item.parentId;
    var root=item;
    for (var i=0;i<this.props.desktopItems.length;i++)
        {
            var obj=this.props.desktopItems[i];
            if (obj.id==parentId)
                {
                    parentId=obj.parentId;
                    root=obj;
                }
        }
    return root;
  }
  getChildren(id)
  {
    //alert("getChildren");
    var newItems=[];
    console.log("getChildren",id);
    if (this.props.desktopItems)
    for (var i=0;i<this.props.desktopItems.length;i++)
        {
            
            if (this.props.desktopItems[i].parentId==id)
            {
                newItems.push(this.props.desktopItems[i]);
            }
        }
        console.log("NewItems=",newItems)
         
  return newItems;
  }
  openWindow(id)
  {
    var item=this.props.desktopItems.find(item=>item.id==id);
    console.log("openWindow=",id,item);
    var changed = Object.assign(
								{
								}, item, {
								
									
									windowShow: true
								}); 
                            	this.props.dispatch(
								{
									type: 'UPDATE_ITEM',
									instance: changed
								});
  }
 
  render() {
   //var items=this.props.item.navStatus;
   var windowItem=this.props.desktopItems.find(item=>item.id==this.props.windowId);
   if (windowItem)
   var navStatus=windowItem.navStatus;
   if (!navStatus) 
   var navStatus=[];
     console.log("MODE=",this.props.mode);
 if (navStatus.length==0)
    {
        var items=this.getChildren(this.props.windowId);
        console.log("root items=",items);
    }
  else
    {
           console.log("#### navStatus items=",navStatus);
         var items=navStatus[navStatus.length-1].items;
         
       console.log("#### navStatus items=",navStatus,items);
      //var items=this.props.item.navStatus[this.props.item.navStatus.length=1].newItems;
    
        
    }
  if (items)
  {
    return (
       
     
        <div class="viewPort">
        {items.map(item => {   
          console.log("Item=",item);
         return(
         
            <DesktopItem
            showContextMenu={this.props.showContextMenu}
            item={ item}
            fileStatus={item.fileStatus}
            mode={this.props.mode}
            windowId={this.props.windowId}
            
            openWindow={this.openWindow}
            createProcess={this.props.createProcess}
            navigateWindow={this.props.navigateWindow}
           
            />
         );
         })}
     
    </div> 
    );
    }
   else
   {
    return (
    <div>
    </div>
    );
   } 
  }}
  
   
 const mapStateToProps = state =>{
return  {
    desktopItems: state.desktopItems,
     desktopMenus: state.desktopMenus
    
        }
	}

const DesktopViewPort = connect(mapStateToProps)(ConnectedDesktopViewPort);
export default DesktopViewPort;
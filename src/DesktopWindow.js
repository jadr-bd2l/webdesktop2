import React, { Component } from 'react';
import { render } from "react-dom";
import {Rnd} from 'react-rnd';
import { Window, TitleBar, Text,NavPane, NavPaneItem } from 'react-desktop/windows';
import DesktopViewPort  from "./DesktopViewPort";
import { Nav,Navbar,NavbarBrand, NavItem, Dropdown, DropdownItem, DropdownToggle, DropdownMenu, NavLink } from 'reactstrap';
import $ from "jquery";
import 'bootstrap/dist/css/bootstrap.min.css';
export class DesktopWindow extends React.Component{
  constructor() 
  {
    super();
    this.state = {
     width:400,
     height:200,
     position:{x:150,y:205},
     status:"normal",
     dropdownOpen: false,
     doFocus:0
    
    
    };
  this.toggle = this.toggle.bind(this); 
  this.tick=this.tick.bind(this);
  this.onCloseClick=this.onCloseClick.bind(this);
  this.onMinimizeClick=this.onMinimizeClick.bind(this);
  this.onMaximizeClick=this.onMaximizeClick.bind(this);
  this.renderNav=this.renderNav.bind(this);
  this.renderPath=this.renderPath.bind(this);
  this.navigatePath=this.navigatePath.bind(this);
  this.focus=this.focus.bind(this);
  this.dedupe=this.dedupe.bind(this);
  this.menuClick=this.menuClick.bind(this);
 // this.renderIcon=this.renderIcon.bind(this);	
        }
    componentDidMount() {
     this.timerID = setInterval(
      () => this.tick(),
      500
    );

  }
  tick()
  {
    if (this.state.doFocus==1)
        {
        this.focus(); 
        this.state.doFocus=0;
        }
  }
  focus()
  {
    var max=0;
    console.log("FOCUS=",this.props.windowId);
    var windowId=this.props.windowId;
    
    $('.windowBounds').each(function(i, obj) {
        var zIndex=parseInt($( obj ).css( "z-index" ));
        if (zIndex>max)
            {max=zIndex;}
        
     });
    $('.windowBounds').each(function(i, obj) {
    
    var zIndex=$( obj ).css( "z-index" );
    console.log("obj=",obj.id,zIndex);
    if ("window"+windowId==obj.id)
        {
            $( obj ).css( "z-index" ,max+1);
        }
    });
  }
  
  menuClick(e){
  
    console.log("menuClick=",e.target.value);
  }
  
  onCloseClick(){
   this.props.closeWindow(this.props.id);
  }
  
  onMinimizeClick()
  {
  this.props.minimizeWindow(this.props.id);  
  }
  onMaximizeClick()
  {
   this.props.maximizeWindow(this.props.id);  
  }
  navigatePath(e)
  {
    console.log(e.target);
    
    var tag=e.target;
    var id=tag.attributes[2].nodeValue;
    console.log("id=",id);
    
    this.props.navigatePath(id);
  }
   toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  dedupe(navStatus)
  {
    var idx=[];
    var newNavStatus=[];
    for (var i=0;i<navStatus.length;i++)
        {
            if (!idx[navStatus[i].id])
                {
                    newNavStatus.push(navStatus[i]);
                    idx[navStatus[i].id]=1;
                }
        }
  return (newNavStatus);      
  }
  
  renderPath()
  {
    var undefined;
    console.log("renderPath:navStatus=",this.props,this.props.navStatus);
    //var navStatus=
    if (this.props.navStatus==undefined)
    var navStatus=[];
    else
     var navStatus=this.dedupe(this.props.navStatus);
     
     
     
    return(
    <div>
    <a class="link"
    href={"#"}
    value={this.props.id}
    onClick={this.navigatePath} 
    > {this.props.title}</a>
    
  
  
   
    {navStatus.map(item => {   
    
    return(
    <span>
    /
    <a class="link"
    href={"#"}
    value={item.id}
    onClick={this.navigatePath} 
    > {item.name}
    </a> 
    </span>
     
   
    );
    
    })}
    </div>
    );
  console.log("navStatus=",this.props.navStatus);
  //return(  <a> {this.props.title}/</a>);
    
  }

  renderNav(menu)
  {
    
    return (
      <div class='windowNav'  >
         <Navbar dark expand="md">
           <NavbarBrand href="/" className="mr-auto"> <table width="260px"></table>     </NavbarBrand>
        <Nav >
         
          <Dropdown nav isOpen={this.state.dropdownOpen} toggle={this.toggle}>
            <DropdownToggle nav caret>
                Actions
            </DropdownToggle>
            <DropdownMenu>
             {menu.map(item => {   
               return(
              <DropdownItem value={item.name} onClick={this.menuClick}>{item.label}</DropdownItem>
              );
         })}
           
            </DropdownMenu>
          </Dropdown>
          
        
        </Nav>
        </Navbar>
        </div>
     
    );
  }
// <DropdownItem divider />
  render() {
  console.log("navStatus=",this.props);
   if (this.props.show==true && this.props.status!="minimized")  
 {
  
 var init=0;
 if (this.state.status=="normal" && this.props.status=="maximized" )
 {
       this.state.doFocus=1;  
    this.state.status="maximized";
    init=1;
 }
 if (this.state.status=="maximized" && this.props.status=="normal" )
 {
      this.state.doFocus=1;  
    this.state.status="normal";
    init=1;
 } 
 
 if (init==1 || this.state.status=="")
 {
 if (this.props.status=="maximized")
 {
    
    var width=window.innerWidth;
    var height=window.innerHeight-83;
    this.state.width=width;
    this.state.height=height;
    var x=0;
    var y=30;
     this.state.position.x=0;
    this.state.position.y=0;
    
 }  
 else
 {
  
    var x=150;
    var y=205;
    
     this.setState({
                width: 400,
                height: 200,
                position:{x:x,y:y}
                });
 } 
}
console.log("WindowId=",this.props.windowId);
var windowId=this.props.windowId;
 if (init==1)
 {
 if (this.props.status!="maximized")
 {
 return(
 <div class="windowBounds" id={"window"+windowId} onClick={this.focus}>
     <Rnd
      default={{
        x: x,
        y: y,
        width: width,
        height: height,
      }}
      size={{ width: this.state.width,  height: this.state.height }}
      position={{ x: this.state.position.x, y: this.state.position.y }}
      onResize={(e, direction, ref, delta, position) => {
        console.log("position=",position);
        this.focus();
            this.setState({
                width: ref.style.width,
                height: ref.style.height,
                position:position
                });
  }}
      minWidth={400}
      minHeight={190}
      bounds="window"
    >
    
      <Window
        color={this.props.color}
        theme="dark"
        class="DesktopWindow"
        chrome={true}
        height={this.state.height}
        width={this.state.width}
        padding="12px"
      >
       
        <TitleBar title={this.renderPath()} controls
        onCloseClick={this.onCloseClick}
       
        background= "royalblue"
        onMaximizeClick={this.onMaximizeClick}
        onMinimizeClick={this.onMinimizeClick}
        />
       {this.renderNav(this.props.menu)}
       
      
      <div class="itemViewPort">
         <DesktopViewPort
        items={this.props.desktopItems}
        showContextMenu={this.props.showContextMenu}
        navigateWindow={this.props.navigateWindow}
        mode="sub"
        windowId={windowId}
        id={this.props.id}
        />
        </div>
      </Window>
    </Rnd>
 </div>
 );
 }
 else
 {
 return(
 <div class="windowBounds" id={"window"+windowId} onClick={this.focus}>
     <Rnd
      default={{
        x: 0,
        y: 0,
        width: this.state.width,
        height: this.state.height,
      }}
      size={{ width: this.state.width,  height: this.state.height }}
      position={{ x: 0, y: 0 }}
     disableDragging
      minWidth={400}
      minHeight={190}
      bounds="window"
    >
    
      <Window
        color={this.props.color}
        theme="dark"
        class="DesktopWindow"
        chrome={true}
        height={this.state.height}
        width={this.state.width}
        padding="12px"
      >
       
        <TitleBar title={this.renderPath()} controls
        onCloseClick={this.onCloseClick}
        background= "royalblue"
        onclick={this.focus}
        onMaximizeClick={this.onMaximizeClick}
        onMinimizeClick={this.onMinimizeClick}
        />
       {this.renderNav(this.props.menu)}
       
      
      <div class="itemViewPort">
         <DesktopViewPort
        items={this.props.desktopItems}
        showContextMenu={this.props.showContextMenu}
        navigateWindow={this.props.navigateWindow}
        createProcess={this.props.createProcess}
        mode="sub"
        id={this.props.id}
        windowId={windowId}
        />
        </div>
      </Window>
    </Rnd>
 </div>
 );   
 }
 }
 else
 {
 return(
 <div class="windowBounds" id={"window"+windowId} onClick={this.focus}>
     <Rnd
      default={{
        x: 150,
        y: 205,
        width: 400,
        height: 200,
      }}
       onResize={(e, direction, ref, delta, position) => {
        console.log("position=",position);
       this.focus();
            this.setState({
                width: ref.style.width,
                height: ref.style.height,
                  position:position
                });
  }}
      minWidth={400}
      minHeight={70}
      bounds="window"
    >
      <Window
        color={this.props.color}
      
        class="DesktopWindow"
        theme="dark"
        chrome={true}
        height={this.state.height}
        width={this.state.width}
        padding="12px"
      >  
        <TitleBar title={this.renderPath()} controls
        onCloseClick={this.onCloseClick}
        onclick={this.focus}
        background= "royalblue"
        onMaximizeClick={this.onMaximizeClick}
        onMinimizeClick={this.onMinimizeClick}
        />
        
  {this.renderNav(this.props.menu)}
  <div class="itemViewPort">
         <DesktopViewPort
        items={this.props.desktopItems}
        showContextMenu={this.props.showContextMenu}
        createProcess={this.props.createProcess}
          navigateWindow={this.props.navigateWindow}
        mode="sub"
         windowId={windowId}
        id={this.props.id}
        />
        </div>
      </Window>
    </Rnd>
 </div>
 );   
 }
 }
 if (this.props.show==false || this.props.status=="minimized")  
 return(
 <div>
 </div>
 );

			
		
   
   
  }
  
 }
  
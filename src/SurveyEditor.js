import React, { Component } from "react";
import * as SurveyJSEditor from "surveyjs-editor";
import * as SurveyKo from "survey-knockout";
import "surveyjs-editor/surveyeditor.css";

import "jquery-ui/themes/base/all.css";
import "nouislider/distribute/nouislider.css";
import "select2/dist/css/select2.css";
import "bootstrap-slider/dist/css/bootstrap-slider.css";

import "jquery-bar-rating/dist/themes/css-stars.css";
import "jquery-bar-rating/dist/themes/fontawesome-stars.css";

import $ from "jquery";
import "jquery-ui/ui/widgets/datepicker.js";
import "select2/dist/js/select2.js";
import "jquery-bar-rating";

import "icheck/skins/square/blue.css";

import * as widgets from "surveyjs-widgets";

widgets.icheck(SurveyKo, $);
widgets.select2(SurveyKo, $);
widgets.inputmask(SurveyKo);
widgets.jquerybarrating(SurveyKo, $);
widgets.jqueryuidatepicker(SurveyKo, $);
widgets.nouislider(SurveyKo);
widgets.select2tagbox(SurveyKo, $);
widgets.signaturepad(SurveyKo);
widgets.sortablejs(SurveyKo);
widgets.ckeditor(SurveyKo);
widgets.autocomplete(SurveyKo, $);
widgets.bootstrapslider(SurveyKo);

class SurveyEditor extends React.Component{
  editor;
    constructor() 
  {
    super();
    this.state = {
    init:0
    
    };
    
    }
  componentDidMount() {
    let editorOptions = { showEmbededSurveyTab: false,showJSONEditorTab: false	};
    this.editor = new SurveyJSEditor.SurveyEditor(
      "surveyEditorContainer",
      editorOptions
    );
    this.editor.saveSurveyFunc = this.saveMySurvey;
    console.log("EDITOR DATA IS",this.props.data );
    //if (this.props.data && this.editor.text=="")
    if (this.state.init==0)
    {
   this.editor.text = this.props.data || "";
    this.state.init=1;
    }
    
  }
  render() {
    
    return <div id="surveyEditorContainer" />;
  }
  saveMySurvey = () => {
    console.log(JSON.stringify(this.editor.text));
    this.props.save(this.editor.text);
  };
}

export default SurveyEditor;

import { ADD_ITEM,UPDATE_ITEM ,SET_ITEMS,SET_MENUS,ADD_PROCESS,KILL_PROCESS,UPDATE_PROCESS,SET_PROCESSES} from "./action-types.js";


export function addItem(payload) {
	console.log(Date.now(),'addItem',payload)
  return { type: ADD_ITEM, payload }
}


export function updateItem(payload) {
console.log(Date.now(),'updateItem',payload)
  return { type: UPDATE_ITEM, payload }
}


export function setItems(payload) {
 console.log(Date.now(),'setItems',payload)
  return { type: SET_ITEMS, payload}
}

export function setMenus(payload) {
 console.log(Date.now(),'setMenus',payload)
  return { type: SET_MENUS, payload}
}

export function addProcess(payload) {
 console.log(Date.now(),'addProcess',payload)
  return { type: ADD_PROCESS, payload}
}

export function setProcesses(payload) {
 console.log(Date.now(),'setProcesses',payload)
  return { type: SET_PROCESSES, payload}
}

export function killProcess(payload) {
 console.log(Date.now(),'killProcess',payload)
  return { type: KILL_PROCESS, payload}
}
export function updateProcess(payload) {
 console.log(Date.now(),'updateProcess',payload)
  return { type: UPDATE_PROCESS, payload}
}
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { Provider } from "react-redux";
import { combineReducers, createStore } from 'redux'
import { addItem,updateItem,setItems } from "./actions.js";


var directory=[];
var functions=[];
//var processes=[{windowShow:true,windowStatus:"maximized",title:"test process",id:"1000"}];
var processes=[];



directory.push({parentId:31,name: "dataset A",type:"dataset",id:2,left:"20px",top:"40px",windowShow:false,windowStatus:"normal",fileStatus:"normal",data:{}}) ;
directory.push({parentId:31,name: "questionnaire A",type:"questionnaire",id:3,left:"100px",top:"40px",windowShow:false,windowStatus:"normal",fileStatus:"normal",data:""}) ;
directory.push({parentId:31,name: "data visualisation A",type:"dashboard",id:4,left:"180px",top:"40px",windowShow:false,windowStatus:"normal",fileStatus:"normal",data:""}) ;
directory.push({parentId:31,name: "Respondent database",type:"mailingList",id:5,left:"260px",top:"40px",windowShow:false,windowStatus:"normal",fileStatus:"normal",data:""}) ;
directory.push({parentId:31,name: "email 1",type:"email",id:6,left:"20px",top:"120px",windowShow:false,windowStatus:"normal",fileStatus:"normal",data:""}) ;
directory.push({parentId:31,name: "hot alert 1",type:"alert",id:7,left:"100px",top:"120px",windowShow:false,windowStatus:"normal",fileStatus:"normal",data:""}) ;
directory.push({parentId:31,name: "image 1",type:"image",id:8,left:"180px",top:"120px",windowShow:false,windowStatus:"normal",fileStatus:"normal",data:""}) ;
directory.push({parentId:31,name: "Panel 1",type:"panel",id:9,left:"260px",top:"120px",windowShow:false,windowStatus:"normal",fileStatus:"normal",data:""}) ;
directory.push({parentId:31,name: "Quotas",type:"quotas",id:10,left:"20px",top:"200px",windowShow:false,windowStatus:"normal",fileStatus:"normal",data:""}) ;

directory.push({parentId:111,name: "Project test1",type:"project",id:31,left:"100px",top:"60px"}) ;
directory.push({parentId:111,name: "Project test2",type:"project",id:32,left:"180px",top:"60px"}) ;
directory.push({parentId:111,name: "folder test3",type:"folder",id:43,left:"260px",top:"60px"}) ;


directory.push({parentId:0,name: "Project C",type:"project",id:21,left:"100px",top:"60px"}) ;
directory.push({parentId:0,name: "Project D",type:"project",id:22,left:"180px",top:"60px"}) ;
directory.push({parentId:0,name: "Project E",type:"project",id:23,left:"260px",top:"60px"}) ;
directory.push({parentId:1,name: "Folder C",type:"folder",id:111,left:"100px",top:"60px"}) ;
directory.push({parentId:1,name: "Project D",type:"project",id:222,left:"180px",top:"60px"}) ;
directory.push({parentId:1,name: "Project E",type:"project",id:333,left:"260px",top:"60px"}) ;


directory.push({parentId:-1,name: "folder a",type:"folder",id:0,left:"20px",top:"100px",windowShow:false,windowStatus:"normal",navStatus:[]}) ;
directory.push({parentId:-1,name: "Project B",type:"project",id:1,left:"100px",top:"100px",windowShow:false,windowStatus:"normal",navStatus:[]}) ;

functions.push({type: "folder",contextMenu:[],menu:[]});
functions[0].contextMenu.push({name:"editFolder",icon:"",label:"Edit folder"});
functions[0].contextMenu.push({name:"deleteFolder",icon:"",label:"Delete folder"});
functions[0].menu.push({name:"createFolder",icon:"",label:"Create folder"});
functions[0].menu.push({name:"createProject",icon:"",label:"Create project"});
functions["folder"]={type: "folder",contextMenu:[],menu:[]};
functions["folder"].contextMenu.push({name:"editFolder",icon:"",label:"Edit folder"});
functions["folder"].contextMenu.push({name:"deleteFolder",icon:"",label:"Delete folder"});
functions["folder"].menu.push({name:"createFolder",icon:"",label:"Create folder"});
functions["folder"].menu.push({name:"createProject",icon:"",label:"Create project"});


functions.push({type: "project",contextMenu:[],menu:[]});


functions["project"]={type: "project",contextMenu:[],menu:[]};
functions["project"].contextMenu.push({name:"editProject",icon:"",label:"Edit project"});
functions["project"].contextMenu.push({name:"deleteProject",icon:"",label:"Delete project"});
functions["project"].menu.push({name:"createDataset",icon:"",label:"Create dataset"});
functions["project"].menu.push({name:"createDashboard",icon:"",label:"Create dahsboard"});
functions["project"].menu.push({name:"createQuestionnaire",icon:"",label:"Create questionnaire"});
functions["project"].menu.push({name:"createMailingList",icon:"",label:"Create mailing list"});
functions["project"].menu.push({name:"createEmail",icon:"",label:"Create email"});
functions["project"].menu.push({name:"createAlert",icon:"",label:"Create alert"});
functions["project"].menu.push({name:"createPanel",icon:"",label:"Create panel"});


functions.push({type: "dataset",contextMenu:[],menu:[]});
functions["dataset"]={type: "dataset",contextMenu:[],menu:[]};
functions["dataset"].contextMenu.push({name:"editDataset",icon:"",label:"Edit dataset"});
functions["dataset"].contextMenu.push({name:"updateDataset",icon:"",label:"update dataset"});
functions["dataset"].contextMenu.push({name:"deleteDataset",icon:"",label:"delete dataset"});

functions.push({type: "questionnaire",contextMenu:[],menu:[]});

functions["questionnaire"]={type: "questionnaire",contextMenu:[],menu:[]};

functions["questionnaire"].contextMenu.push({name:"editQuestionnaire",icon:"",label:"Edit questionnaire"});
functions["questionnaire"].contextMenu.push({name:"viewQuestionnaire",icon:"",label:"View questionnaire"});
functions["questionnaire"].contextMenu.push({name:"deleteQuestionnaire",icon:"",label:"delete questionnaire"});


functions.push({type: "dashboard",contextMenu:[],menu:[]});
functions["dashboard"]={type: "dashboard",contextMenu:[],menu:[]};
functions["dashboard"].contextMenu.push({name:"editDashboard",icon:"",label:"Edit dashboard"});
functions["dashboard"].contextMenu.push({name:"viewDashboard",icon:"",label:"View dashboard"});
functions["dashboard"].contextMenu.push({name:"deleteDashboard",icon:"",label:"Delete dashboard"});


functions.push({type: "mailingList",contextMenu:[],menu:[]});
functions["mailingList"]={type: "mailingList",contextMenu:[],menu:[]};
functions["mailingList"].contextMenu.push({name:"editMailingList",icon:"",label:"Edit mailing list"});
functions["mailingList"].contextMenu.push({name:"deleteMailingList",icon:"",label:"Delete mailing list"});


functions.push({type: "email",contextMenu:[],menu:[]});
functions["email"]={type: "email",contextMenu:[],menu:[]};
functions["email"].contextMenu.push({name:"editEmail",icon:"",label:"Edit email"});
functions["email"].contextMenu.push({name:"deleteEmail",icon:"",label:"Delete email"});


functions.push({type: "alert",contextMenu:[],menu:[]});

functions["alert"]={type: "alert",contextMenu:[],menu:[]};
functions["alert"].contextMenu.push({name:"editAlert",icon:"",label:"Edit alert"});
functions["alert"].contextMenu.push({name:"deleteAlert",icon:"",label:"Delete alert"});

functions.push({type: "image",contextMenu:[],menu:[]});
functions["image"]={type: "image",contextMenu:[],menu:[]};
functions["image"].contextMenu.push({name:"editImage",icon:"",label:"Edit image"});
functions["image"].contextMenu.push({name:"deleteImage",icon:"",label:"Delete image"});


functions.push({type: "panel",contextMenu:[],menu:[]});
functions["panel"]={type: "panel",contextMenu:[],menu:[]};
functions["panel"].contextMenu.push({name:"editPanel",icon:"",label:"Edit panel"});
functions["panel"].contextMenu.push({name:"deletePanel",icon:"",label:"Delete panel"});



functions.push({type: "quotas",contextMenu:[],menu:[]});



functions["quotas"]={type: "quotas",contextMenu:[],menu:[]};
functions["quotas"].contextMenu.push({name:"editQuotas",icon:"",label:"Edit quotas"});
functions["quotas"].contextMenu.push({name:"deleteQuotas",icon:"",label:"Delete quotas"});

function desktopItems(state=[],action){
	switch (action.type){
		case 'ADD_ITEM':
			return [...state, action.instance]
		case 'UPDATE_ITEM':
            console.log("UPDATE_SCHEDULE_ITEM")
			//console.log({scheduleItems:action.type,instance:action.instance})
			const index = state.findIndex(item => item.id === action.instance.id)
			//console.log({index:index})
			const result = [
           ...state.slice(0, index), // everything before
					 action.instance,
           ...state.slice(index + 1), // everything after
			]
			//console.log(result)
			return result
		case 'SET_ITEMS':
			return action.payload
		default:
			return state
	}
}

function desktopMenus(state=[],action){
	switch (action.type){
	
		case 'SET_MENUS':
			return action.payload
		default:
			return state
	}
    }
    
function desktopProcesses(state=[],action){
	switch (action.type){
	
		case 'ADD_PROCESS':
			return [...state, action.instance]
        case 'UPDATE_PROCESS':
        
          
			//console.log({scheduleItems:action.type,instance:action.instance})
			const index = state.findIndex(item => item.id === action.instance.id)
			//console.log({index:index})
			const result = [
           ...state.slice(0, index), // everything before
					 action.instance,
           ...state.slice(index + 1), // everything after
			]
			//console.log(result)
			return result
        case 'KILL_PROCESS':
        const index2 = state.findIndex(item => item.id === action.instance.id)
        const result2 = [
           ...state.slice(0, index2), // everything before
           ...state.slice(index2 + 1), // everything after
			]
			//console.log(result)
			return result2
        case 'SET_PROCESSES':
			return action.payload    
		default:
			return state
	}    
    
}
const reducer = combineReducers({ desktopItems,desktopMenus,desktopProcesses});
const store = createStore(reducer);

const unsubscribe = store.subscribe(() =>
  console.log(store.getState())
)


window.store = store;
window.addItem = addItem;

store.dispatch({type: 'SET_ITEMS',payload: directory});
store.dispatch({type: 'SET_MENUS',payload: functions});
store.dispatch({type: 'SET_PROCESSES',payload: processes});
console.log("STORE=",store.getState());
ReactDOM.render(
<Provider store={store}>
<App />
</Provider>
, document.getElementById('root')

);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA


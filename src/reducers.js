import { ADD_ITEM,UPDATE_ITEM,SET_ITEMS} from "./action-types.js";
const initialState = {
  directory: []
};
function rootReducer(state = initialState, action) {
  if (action.type === ADD_ITEM) {
    return Object.assign({}, state, {
      items: state.directory.concat(action.payload)
    });
  }
  if (action.type === UPDATE_ITEM) {
   const index = state.findIndex(item => item.id === action.payload.id)
			//console.log({index:index})
	const result = [
           ...state.slice(0, index), // everything before
					 action.instance,
           ...state.slice(index + 1), // everything after
			]
			//console.log(result)
			return result
  }
   if (action.type === SET_ITEMS) {
    return action.payload;
    
    }
  return state;
}

export default rootReducer;